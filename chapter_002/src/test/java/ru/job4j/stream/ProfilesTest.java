package ru.job4j.stream;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProfilesTest {

    @Test
    public void whenCollectAddressesToList() {
        Profiles profiles = new Profiles();
        Address address1 = new Address("Moscow", "Lenin", 1, 11);
        Address address2 = new Address("Paris", "St.Lois", 6, 101);
        Address address3 = new Address("Madrid", "Marina", 3, 23);
        Address address4 = new Address("Saint-Petersburg", "Engels", 9, 4);
        Profile profile1 = new Profile(address1);
        Profile profile2 = new Profile(address2);
        Profile profile3 = new Profile(address3);
        Profile profile4 = new Profile(address4);
        List<Profile> profileList = Arrays.asList(profile1, profile2, profile3, profile4);
        List<Address> expected = profiles.collect(profileList);
        assertThat(expected, is(Arrays.asList(address3, address1, address2, address4)));
    }

    @Test
    public void whenCollectUniqueAddressesToList() {
        Profiles profiles = new Profiles();
        Address address1 = new Address("A-city", "Blue", 2, 22);
        Address address2 = new Address("C-city", "Yellow", 1, 11);
        Address address3 = new Address("B-city", "Red", 3, 33);
        Address address4 = new Address("A-city", "Green", 4, 44);
        Address address5 = new Address("B-city", "Black", 5, 55);
        Profile profile1 = new Profile(address1);
        Profile profile2 = new Profile(address2);
        Profile profile3 = new Profile(address3);
        Profile profile4 = new Profile(address4);
        Profile profile5 = new Profile(address5);
        List<Profile> profileList = Arrays.asList(profile1, profile2, profile3, profile4, profile5);
        List<Address> expected = profiles.collect(profileList);
        assertThat(expected, is(Arrays.asList(address1, address3, address2)));
    }

    @Test
    public void whenCollectWithoutDuplicatesAddressesToList() {
        Profiles profiles = new Profiles();
        Address address1 = new Address("A-city", "Blue", 2, 22);
        Address address2 = new Address("C-city", "Yellow", 1, 11);
        Address address3 = new Address("A-city", "Blue", 2, 22);
        Address address4 = new Address("B-city", "Red", 3, 33);
        Address address5 = new Address("C-city", "Yellow", 1, 11);
        Address address6 = new Address("B-city", "Red", 3, 33);
        Profile profile1 = new Profile(address1);
        Profile profile2 = new Profile(address2);
        Profile profile3 = new Profile(address3);
        Profile profile4 = new Profile(address4);
        Profile profile5 = new Profile(address5);
        Profile profile6 = new Profile(address6);
        List<Profile> profileList = Arrays.asList(profile1, profile2, profile3, profile4, profile5);
        List<Address> expected = profiles.collect(profileList);
        assertThat(expected, is(Arrays.asList(address1, address4, address2)));
    }
}
