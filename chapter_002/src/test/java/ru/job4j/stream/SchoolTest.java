package ru.job4j.stream;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class SchoolTest {
    private List<Student> students = new ArrayList<>();

    @Before
    public void setUp() {
        students.add(new Student(10, "Surname1"));
        students.add(new Student(20, "Surname2"));
        students.add(new Student(30, "Surname3"));
        students.add(new Student(40, "Surname4"));
        students.add(new Student(50, "Surname5"));
        students.add(new Student(60, "Surname6"));
        students.add(new Student(70, "Surname7"));
        students.add(new Student(80, "Surname8"));
        students.add(new Student(90, "Surname9"));
    }

    @Test
    public void whenCollectClassA() {
        School sc = new School();
        Predicate<Student> pr = student -> student.getScore() >= 70 && student.getScore() <= 100;;
        List<Student> rsl = sc.collect(students, pr);
        List<Student> expected = new ArrayList<>();
        expected.add(new Student(70, "Surname7"));
        expected.add(new Student(80, "Surname8"));
        expected.add(new Student(90, "Surname9"));
        assertThat(rsl, is(expected));
    }

    @Test
    public void whenCollectClassB() {
        School sc = new School();
        Predicate<Student> pr = student -> student.getScore() >= 50 && student.getScore() < 70;
        List<Student> rsl = sc.collect(students, pr);
        List<Student> expected = new ArrayList<>();
        expected.add(new Student(50, "Surname5"));
        expected.add(new Student(60, "Surname6"));
        assertThat(rsl, is(expected));
    }

    @Test
    public void whenCollectClassC() {
        School sc = new School();
        Predicate<Student> pr = student -> student.getScore() > 0 && student.getScore() < 50;
        List<Student> rsl = sc.collect(students, pr);
        List<Student> expected = new ArrayList<>();
        expected.add(new Student(10, "Surname1"));
        expected.add(new Student(20, "Surname2"));
        expected.add(new Student(30, "Surname3"));
        expected.add(new Student(40, "Surname4"));
        assertThat(rsl, is(expected));
    }

    @Test
    public void whenCollectListOfStudentsToMap() {
        School school = new School();
        List<Student> st = new ArrayList<>();
        Student student1 = new Student(100, "Aa");
        Student student2 = new Student(90, "Dd");
        Student student3 = new Student(80, "Ff");
        Student student4 = new Student(75, "Oo");
        st.add(student1);
        st.add(student2);
        st.add(student3);
        st.add(student4);
        Map<String, Student> expected = new HashMap<>();
        expected.put("Aa", new Student(100, "Aa"));
        expected.put("Dd", new Student(90, "Dd"));
        expected.put("Ff", new Student(80, "Ff"));
        expected.put("Oo", new Student(75, "Oo"));
        assertThat(school.collectToMap(st), is(expected));
    }

    @Test
    public void whenCollectListOfUniqueStudentsToMap() {
        School school = new School();
        List<Student> st = new ArrayList<>();
        Student student1 = new Student(100, "Aa");
        Student student2 = new Student(100, "Aa");
        Student student3 = new Student(90, "Dd");
        Student student4 = new Student(90, "Dd");
        Student student5 = new Student(75, "Oo");
        Student student6 = new Student(75, "Oo");
        st.add(student1);
        st.add(student2);
        st.add(student3);
        st.add(student4);
        st.add(student5);
        st.add(student6);
        Map<String, Student> expected = new HashMap<>();
        expected.put("Aa", new Student(100, "Aa"));
        expected.put("Dd", new Student(90, "Dd"));
        expected.put("Oo", new Student(75, "Oo"));
        assertThat(school.collectToMap(st), is(expected));
    }
}