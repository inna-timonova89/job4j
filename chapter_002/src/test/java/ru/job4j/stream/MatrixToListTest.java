package ru.job4j.stream;

import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MatrixToListTest {

    @Test
    public void whenConvertMatrixToList() {
        MatrixToList list = new MatrixToList();
        Integer[][] matr = {{1, 2}, {3, 4}};
        List<Integer> expected = Arrays.asList(1, 2, 3, 4);
        List<Integer> rsl = list.matrixToList(matr);
        assertThat(rsl, is(expected));
    }

}
