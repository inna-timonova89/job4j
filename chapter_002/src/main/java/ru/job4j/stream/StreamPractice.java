package ru.job4j.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamPractice {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(-1, 1, -2, 2, -3, 3));

        List<Integer> nums = numbers.stream().filter(
                integer -> integer > 0).collect(Collectors.toList());
        nums.forEach(System.out :: println);
    }
}
