package ru.job4j.stream;

import jdk.jfr.DataAmount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class EasyStream {

    private List<Integer> source = new ArrayList<>();


    public static EasyStream of(List<Integer> source) {
        return EasyStream.builder()
                .source(source)
                .build();
    }

    public EasyStream map(Function<Integer, Integer> fun) {
        List<Integer> list = new ArrayList<>();
        for (Integer el : source) {
            list.add(fun.apply(el));
        }
        return EasyStream.builder()
                .source(list)
                .build();
    }

    public EasyStream filter(Predicate<Integer> fun) {
        List<Integer> list = new ArrayList<>();
        for (Integer el : source) {
            if (fun.test(el)) list.add(el);
        }
        return EasyStream.builder()
                .source(list)
                .build();
    }

    public List<Integer> collect() {
        return this.getSource();
    }
}