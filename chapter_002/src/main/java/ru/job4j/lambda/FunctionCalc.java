package ru.job4j.lambda;

import java.util.ArrayList;
import java.util.List;

public class FunctionCalc {

    interface Function<T, D extends Number> {
        T apply(T t);
    }

    List<Double> diapason(int start, int end, Function<Double, Double> func) {
        List<Double> rsl = new ArrayList<>();
        for(int i = start; i < end; i++) {
            rsl.add(func.apply((double) i));
        }
        return rsl;
    }
}
