package ru.job4j.bank;

import java.util.*;
import java.util.stream.Collectors;

public class BankService {
    private Map<User, List<Account>> users = new HashMap<>();

    public void addUser(User user) {
        if (user != null) {
            users.putIfAbsent(user, new ArrayList<>());
        }
    }

    public void addAccount(String passport, Account account) {
        List<Account> accounts = users.get(findByPassport(passport));
        if (account != null && accounts != null) {
            if (!accounts.contains(account)) {
                accounts.add(account);
            }
        }
    }

    public User findByPassport(String passport) {
//        User found = null;
//        Set<User> userSet = users.keySet();
//        for (User u : userSet) {
//            if (u != null) {
//                if (u.getPassport().equals(passport)) {
//                    found = u;
//                    break;
//                }
//            }
//        }
//        return found;
        Optional<User> u = users.keySet().stream().filter(o -> o.getPassport().equals(passport)).findFirst();
        return u.orElseGet(() -> u.orElse(null));
    }

    public List<Account> getUserAccounts(String passport) {
        return users.get(findByPassport(passport));
    }

    public Account findByRequisite(String passport, String requisite) {
        Account rsl = null;
        User byPassport = findByPassport(passport);
        if (byPassport != null) {
//            List<Account> accounts = users.get(byPassport);
//            for (Account account : accounts) {
//                if (account.getRequisite().equals(requisite)) {
//                    rsl = account;
//                    break;
//                }
//            }
            List<Account> accountList = getUserAccounts(passport);
            rsl = accountList.stream().filter(account -> account.getRequisite().equals(requisite)).findFirst().orElse(null);
            return rsl;
        }
        return rsl;
    }

    public boolean transferMoney(String srcPassport, String srcRequisite,
                                 String destPassport, String destRequisite, double amount) {
        boolean rsl = false;
        Account src = findByRequisite(srcPassport, srcRequisite);
        Account dest = findByRequisite(destPassport, destRequisite);
        if (src != null && dest != null && src.getBalance() >= amount) {
            src.setBalance(src.getBalance() - amount);
            dest.setBalance(dest.getBalance() + amount);
            rsl = true;
        }
        return rsl;
    }
}
