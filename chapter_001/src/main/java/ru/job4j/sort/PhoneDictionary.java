package ru.job4j.sort;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.Comparator;

public class PhoneDictionary {
    private ArrayList<Person> persons = new ArrayList<Person>();


    public void add(Person person) {
        this.persons.add(person);
    }

    /**
     * Вернуть список всех пользователей, который содержат key в любых полях.
     *
     * @param key Ключ поиска.
     * @return Список подощедщих пользователей.
     */
//    public ArrayList<Person> find(String key) {
//        Predicate<Person> compareName = person -> person.getName().contains(key);
//        Predicate<Person> compareSurname = person ->  person.getSurname().contains(key);
//        Predicate<Person> comparePhone = person -> person.getPhone().contains(key);
//        Predicate<Person> compareAddress = person -> person.getAddress().contains(key);
//        Predicate<Person> combine = compareName.or(compareSurname).or(comparePhone).or(compareAddress);
//        ArrayList<Person> result = new ArrayList<>();
//        for (Person person : persons) {
//            if(combine.test(person)) {
//                result.add(person);
//            }
//        }
//        return result;
//    }

    public ArrayList<Person> find(String key) {
        var result = new ArrayList<Person>();
        for (Person person : persons) {
            if (person.getName().contains(key) || person.getSurname().contains(key) || person.getPhone().contains(key) || person.getAddress().contains(key)) {
                result.add(person);
            }
        }
        return result;
    }
}
