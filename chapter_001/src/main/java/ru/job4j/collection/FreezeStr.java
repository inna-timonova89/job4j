package ru.job4j.collection;

import java.util.HashMap;

public class FreezeStr {
    public static boolean eq(String left, String right) {
        boolean result = false;
        char[] first = left.toCharArray();
        char[] second = right.toCharArray();
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        for (char c : first) {
            int count = 1;
            final Integer integer = map.putIfAbsent(c, 1);
            if(integer != null) {
                count = map.get(c);
                count++;
            }
            map.put(c, count);
        }
        for(int i = 0; i < second.length; i++) {
            char c = right.charAt(i);
            if (!map.containsKey(c)) {
                return false;
            }
            int count = map.get(c);
            if (count == 1) {
                map.remove(c);
            } else {
                map.put(c, --count);
            }
        }
            if (map.isEmpty()) {
                result = true;
            }
        return result;
    }
}
