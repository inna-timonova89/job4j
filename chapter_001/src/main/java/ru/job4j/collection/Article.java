package ru.job4j.collection;

/**
 * @author inna.timonova (ms.timonovai@mail.ru)
 */

import java.util.Arrays;
import java.util.HashSet;

public class Article {
    public static boolean generateBy(String origin, String line) {
        boolean result = true;
        origin.replaceAll("\\p{P}", "");
        String[] originSplit = origin.split("");
        String[] lineSplit = line.split("");
        HashSet<String> originSet = new HashSet<>(Arrays.asList(originSplit));
        for (String st : lineSplit) {
            if (!originSet.contains(st)) {
                result = false;
            }
        }
        return result;
    }
}
